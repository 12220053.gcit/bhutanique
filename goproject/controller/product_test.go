package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)


func TestAddProduct(t *testing.T) {
	url := "http://localhost:8081/product"
	var data = []byte(`{"pid":1, "name": "dress", "price":40,"picture":"http//gsfwfw",}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content_Type", "application/json")

	// Create client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	assert.JSONEq(t, `{
		"Error": "invalid character '}' looking for beginning of object key string"
	}`, string(body))
}




	
func TestDeleteProduct(t *testing.T) {
	url := "http://localhost:8081/product/2"
	req, _ := http.NewRequest("DELETE", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)

	expected := `{"status": "Deleted"}`
	assert.JSONEq(t, expected, string(body))
}


func TestProductNotFound(t *testing.T) {
	assert := assert.New(t)
	c := http.Client{}
	r, _ := c.Get("http://localhost:8081/product/700")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusBadRequest, r.StatusCode)

	expected := `{"Error": "sql: no rows in result set"}`
	assert.JSONEq(expected, string(body))
}

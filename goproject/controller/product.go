// ==========================================================================================

package controller

import (
	"database/sql"
	"encoding/json"
	postgres "goproject/datastore/Postgress"
	"goproject/model"
	"goproject/utils/httpResp"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)


func AddProduct(w http.ResponseWriter, r *http.Request){

	var product model.Product
	decoder:=json.NewDecoder(r.Body)

	err:=decoder.Decode(&product)
	if err!= nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	saveErr := Create(product)
	if saveErr!=nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusCreated, map[string]string{"status": "Product Added"})
}
// helper function
func Create(p model.Product) error {
	if err := p.Save(); err != nil {
		return err
	}
	return nil
}

// get product
func GetProduct(w http.ResponseWriter, r *http.Request) {
	pidStr := mux.Vars(r)["pid"]
	pid, err := strconv.Atoi(pidStr)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid product ID")
		return
	}

	art, getErr := Read(pid)

	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, art)
}

func Read(pid int) (*model.Product, error) {
	result := &model.Product{Pid: pid}

	if err := result.GetProduct(); err != nil {
		return nil, err
	}
	log.Println(result)
	return result, nil
}


// delete handler
func DeleteProduct(w http.ResponseWriter, r *http.Request) {
	pidStr := mux.Vars(r)["pid"]
	pid, err := strconv.Atoi(pidStr)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid product ID")
		return
	}
	deleteErr := DeleteArt(pid) // Changed variable name to deleteErr

	if deleteErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, deleteErr.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"status": "Deleted"})
}

func DeleteArt(pid int) error {
	current, err := Read(pid)
	if err != nil {
		return err
	}
	return current.Delete()
}




// update
// func UpdateProduct(w http.ResponseWriter, r *http.Request){
// 	old_pid := mux.Vars(r)["pid"]
// 	old_Pid, idErr:=getuseremail(old_pid)
// 	if idErr != nil{
// 		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
// 		return
// 	}
// 	var pro model.Product
// 	decoder:= json.NewDecoder(r.Body)
// 	if err:= decoder.Decode(&pro); err!=nil{
// 		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
// 		return
// 	}
// 	defer r.Body.Close()
// 	updateErr := pro.Update(old_Pid)
// 	if updateErr!= nil{
// 		switch updateErr{
// 			case sql.ErrNoRows:
// 				httpResp.RespondWithError(w, http.StatusNotFound, "user not found")
// 			default:
// 				httpResp.RespondWithError(w,http.StatusInternalServerError, updateErr.Error())
// 		}
// 		}else{
// 			httpResp.ResponseWithJson(w, http.StatusOK,pro)
// 	}
// 	}
func UpdateProduct(w http.ResponseWriter, r *http.Request) {
	oldPidStr := mux.Vars(r)["pid"]
	oldPid, idErr := strconv.Atoi(oldPidStr)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	var pro model.Product
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&pro); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid JSON body")
		return
	}
	defer r.Body.Close()

	updateErr := pro.Update(oldPid)
	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "picture not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
	} else {
		httpResp.ResponseWithJson(w, http.StatusOK, pro)
	}
}






// getall
func GetAllProduct(w http.ResponseWriter, r *http.Request) {
	products, getErr := GetAll()

	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusCreated, products)
}
func GetAll() ([]model.Product, error) {
	rows, getErr := postgres.Db.Query("Select * from product;")
	if getErr != nil {
		return nil, getErr
	}
	products := []model.Product{}

	for rows.Next() {
		product :=model.Product{}
		dbErr := rows.Scan(&product.Pid,&product.Name ,&product.Price, &product.Picture)
		if dbErr != nil {
			return nil, dbErr
		}
		products = append(products, product)
	}
	rows.Close()
	return products, nil
}


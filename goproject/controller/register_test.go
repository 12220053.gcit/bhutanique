package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)


func TestAdminLoginUserExit(t *testing.T) {
	url := "http://localhost:8081/login"
	data := []byte(`{"email":"sc1@gmail.com", "password":"sangay"}`)

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, http.StatusOK, resp.StatusCode)
	assert.Contains(t, string(body), "success")
}

func TestAdmLoginUserNotExit(t *testing.T){
	url:= "http://localhost:8081/login"
	var data =[]byte(`{"email":"scw@gmail.com", "password":"sangay"}`)
	//create request object
	req,_:=http.NewRequest("POST",url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")
	// create client
	client:=&http.Client{}
	//send POST request here using DO func
	resp, err:=client.Do(req)
	if err!= nil{
		panic(err)
	}
	defer resp.Body.Close() //defer the execution of that particulaer program.  execute only when function is aboout to terminate
	body,_:=io.ReadAll(resp.Body) 

	assert.Equal(t, http.StatusUnauthorized, resp.StatusCode)
	assert.JSONEq(t,`{
		"Error": "sql: no rows in result set"
	  }`,string(body))


}
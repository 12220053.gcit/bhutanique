create table product(
    pid int not null,
    name varchar(35) not null,
    price int not null,
    picture text not null,
    primary key(pid)
)
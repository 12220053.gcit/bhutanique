const userEmail = document.getElementById("email").value

function Login() {
  var _data = {
    email : document.getElementById("email").value,
    password : document.getElementById("Password").value
  }
  
  console.log("Sending login request with data:", _data);
  
  fetch('/login', {
    method: "POST",
    body: JSON.stringify(_data),
    headers: {"Content-type": "application/json; charset=UTF-8"}
  })
  
      .then(response => {
        if (response.ok) {
        localStorage.setItem("email", _data.email);
        window.open("index.html", "_self")
        } else {
        throw new Error(response.statusText)
        }
        }).catch(e => {
        if (e == "Error: Unauthorized") {
        alert(e+ ". Credentials does not match!")
        return
        }
    });
    }


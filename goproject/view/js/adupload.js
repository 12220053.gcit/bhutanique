window.onload = function(){
    fetch("/products")
        .then(response => response.text())
        .then(data => GetAllProduct(data))

        
}

const form = document.getElementById("uploadForm")

form.addEventListener("submit", (e) =>{
    e.preventDefault()
    addProduct()
})

document.querySelector("#picture").addEventListener("change", function(){
   
    const dataurl = new FileReader();
    dataurl.addEventListener("load", ()=>{
        localStorage.setItem("image-dataurl", dataurl.result)
    })
    dataurl.readAsDataURL(this.files[0])
})


async function addProduct(){
    // alert(localStorage.getItem("image-dataurl"))
    alert('upload success')
    var data = {
        Pid:parseInt(document.getElementById("id").value),
        Name:document.getElementById("name").value,
        Price:parseInt(document.getElementById("price").value),
        Picture:localStorage.getItem("image-dataurl")
    }

    console.log(data)

    fetch("/product",{
        method: "POST",
        body : JSON.stringify(data),
        headers :{"Content-type" : "application/json; charset = UTF-8"}
    })

    ResetForm()
}

function ResetForm(){
    document.getElementById("id").value = "";
    document.getElementById("name").value = "";
    document.getElementById("price").value = "";
    document.getElementById("picture").value ="";
}







// ====================================================




async function GetAllProduct(data) {
    const products = JSON.parse(data);
    products.forEach((product) => {
      var table = document.getElementById("table");
      var row = table.insertRow();
      var td = [];
  
      for (i = 0; i < table.rows[0].cells.length; i++) {
        td[i] = row.insertCell(i);
      }
      td[0].innerHTML = product.Pid;
      td[1].innerHTML = product.Name;
      td[2].innerHTML = product.Price;
      td[3].innerHTML = '<input type="button" onclick="DeleteProduct(this)" value="Delete" id="buttonDelete">';
      td[4].innerHTML = '<input type="button" onclick="UpdateProduct(this)" value="Update" id="buttonDelete">';
    });
  }
  
  const DeleteProduct = async (r) => {
    if (confirm("Are you sure you want to DELETE this?")) {
      const selectedRow = r.parentElement.parentElement;
      const pid = selectedRow.cells[0].innerHTML;
  
      await fetch(`/product/${pid}`, {
        method: "DELETE",
        headers: { "Content-Type": "application/json; charset=UTF-8" },
      });
  
      const rowIndex = selectedRow.rowIndex;
      if (rowIndex > 0) {
        document.getElementById("table").deleteRow(rowIndex);
      }
    }
  };
  

//   ====update
const UpdateProduct = async (r) => {
    const selectedRow = r.parentElement.parentElement;
    const pid = selectedRow.cells[0].innerHTML;
    const intPid = parseInt(pid);

    // Prompt the user to enter the updated values for the product
    const updatedName = prompt("Enter the updated name:");
    const updatedPrice = prompt("Enter the updated price:");

    const intUpdatedPrice = parseInt(updatedPrice)
    // Create an object with the updated values

    console.log(localStorage.getItem("image-dataurl"))
    var updatedProduct = {
      Pid: intPid,
      Name: updatedName,
      Price: intUpdatedPrice,
      Picture: localStorage.getItem("image-dataurl"),
    };

    console.log(updatedProduct)
  
    fetch("/product/" + pid, {
      method: "PUT",
      body: JSON.stringify(updatedProduct),
      headers: { "Content-Type": "application/json; charset=UTF-8" },
    }).then((res) => {
      if (res.ok) {

        // Update the table row with the updated values
        selectedRow.cells[1].innerHTML = updatedName;
        selectedRow.cells[2].innerHTML = updatedPrice;
      }
    });
  };














  


  

window.onload = function() {
  userSessionEmail = localStorage.getItem('email')
  fetch('/register/' + userSessionEmail)
    .then(response => response.json()) // Parse the response as JSON
    .then(data => {
      console.log(data);
      GetUser(data);
    })
    .catch(error => {
      console.error('Error fetching data:', error);
    });
}

function GetUser(data) {
  try {
    console.log(data)
    const users = data; // Assuming the response is already an array of user objects
    // const usersession = JSON.parse(data);

    var name = document.getElementById('username');
    var email = document.getElementById('email');
    var phone = document.getElementById('phoneNo');

    name.textContent = users.Username;
    email.textContent = users.Email;
    phone.textContent = users.Phone_Number;

  } catch (error) {
    console.error('Error parsing JSON:', error);
  }
}


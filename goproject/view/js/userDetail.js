const userName = document.getElementById("username");
const userEmail = document.getElementById("email");
const userPhone = document.getElementById("phoneNo")
const loggedEmail = localStorage.getItem("email");

window.onload = function () {
    fetch("/register/" + loggedEmail)
    .then((response) => response.text())
    .then((data) => displayUser(data))
}

async function displayUser(data) {
    const user = JSON.parse(data)
    userName.innerHTML = user.Username;
    userEmail.innerHTML = user.Email;
    userPhone.innerHTML = user.Phone_Number;
}
window.onload = function(){
    fetch("/registers")
        .then(response => response.text())
        .then(data => GetAllUsers(data))
    
    fetch("/products")
        .then(response => response.text())
        .then(data => GetAllProduct(data))

       
}



async function GetAllUsers(data){
    var count = 0;
    const users = JSON.parse(data)
    users.forEach(user =>{
        count++
    })
    document.getElementById("user").innerHTML = count
}

async function GetAllProduct(data){
    var count = 0;
    const arts = JSON.parse(data)
    arts.forEach(art =>{
        count++
    })
    document.getElementById("art").innerHTML = count
}
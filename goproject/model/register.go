package model

import postgres "goproject/datastore/Postgress"

type User struct {
	Username     string
	Email        string
	Phone_Number int64
	Password     string
}
const(
	queryInsertUser= "INSERT INTO  registration(username,email,phone_number,password) VALUES($1,$2,$3,$4)"
	queryGetUser="SELECT username, email, phone_number, password FROM registration WHERE email = $1;"
	queryUpdateUser= "UPDATE registration SET username=$1, email=$2, phone_number=$3, password=$4 WHERE email=$5 RETURNING email"
	queryDeleteUser="DELETE FROM registration WHERE email=$1;"
	queryGetuser= "SELECT email, password FROM registration WHERE email=$1 and password=$2;"
)

func(r *User) Create() error{
	_,err:=postgres.Db.Exec(queryInsertUser, r.Username, r.Email, r.Phone_Number,r.Password)
	return err
}
func(r *User) Read() error{
	row :=postgres.Db.QueryRow(queryGetUser,r.Email)
	err :=row.Scan(&r.Username,&r.Email, &r.Phone_Number,&r.Password)
	return err
}
func(r *User)Update(old_email string) error{
	err:=postgres.Db.QueryRow(queryUpdateUser, r.Username, r.Email, r.Phone_Number, r.Password,  old_email).Scan(&r.Email)
	return err
}
func(r *User) Delete()error{
	if _, err:= postgres.Db.Exec(queryDeleteUser, r.Email); err !=nil{
		return err
	}
	return nil
}

// getall

func GetAllUser() ([]User, error){ // no particular receiver because we need a set of students not particular
	rows, err :=postgres.Db.Query("SELECT * FROM registration;") // directly passing from here
	// error handling
	if err != nil{
		return nil, err
	}
//    slice of type student
	users := []User{} // empty slice of type student

	// iterate rows
	for rows.Next(){  // next iterates rows one by one
		var u User
		dbErr:=rows.Scan(&u.Username,&u.Email,&u.Phone_Number,&u.Password) // scan - store the values in s
		if dbErr != nil{
			return nil, dbErr
		}
		users=append(users, u) // adding data in []student using append
	}

	rows.Close()
	return users, nil
}

func (u *User) Get() error{
		// fmt.Println(s)
	err :=postgres.Db.QueryRow(queryGetuser,u.Email,u.Password).Scan (&u.Email,&u.Password)
	// we use .Exec when we don't want anything to be returned
	return err
}

// =============================

// package model

// import postgres "goproject/datastore/Postgress"

// type User struct {
// 	Username     string
// 	Email        string
// 	Phone_Number int64
// 	Password     string
// }

// const (
// 	queryInsertUser = "Insert into registration(username,email,phone_number,password) VALUES($1,$2,$3,$4)"
// 	queryGetUser    = "Select username,email,phone_number,password from registration where email=$1"
// 	queryUpdateUser = "UPDATE registration SET username=$1, email=$2, phone_number=$3, password=$4 where email=$5;"
// 	queryDeleteUser = "DELETE from registration where email= $1;"
	
// )

// func (regUser *User) Save() error {
// 	stmt, err := postgres.Db.Prepare(queryInsertUser)
// 	if err != nil {
// 		return err
// 	}
// 	defer stmt.Close()
// 	_, saveErr := stmt.Exec(regUser.Username, regUser.Email, regUser.Phone_Number, regUser.Password)
// 	if saveErr != nil {
// 		return saveErr
// 	}
// 	return nil
// }

// func (gUser *User) GetUsers() error {
// 	stmt, err := postgres.Db.Prepare(queryGetUser)
// 	if err != nil {
// 		return err
// 	}
// 	defer stmt.Close()
// 	result := stmt.QueryRow(gUser.Email)
// 	getErr := result.Scan(&gUser.Username, &gUser.Email, &gUser.Phone_Number, &gUser.Password)
// 	if getErr != nil {
// 		return getErr
// 	}
// 	return nil
// }
// // 
// func (regUser *User) Update(email string) error {
// 	stmt, updateErr := postgres.Db.Prepare(queryUpdateUser)

// 	if updateErr != nil {
// 		return updateErr
// 	}
// 	defer stmt.Close()

// 	_, updateErr = stmt.Exec(regUser.Username, regUser.Email, regUser.Phone_Number, regUser.Password, email)

// 	if updateErr != nil {
// 		return updateErr
// 	}
// 	return nil
// }



// // delete
// func (demail *User) Delete() error {
// 	stmt, err := postgres.Db.Prepare(queryDeleteUser)

// 	if err != nil {
// 		return err
// 	}
// 	defer stmt.Close()

// 	if _, err = stmt.Exec(demail.Email); err != nil {
// 		return err
// 	}
// 	return nil
// }


// package model

// import postgres "goproject/datastore/Postgress"

// type Product struct {
// 	Name   string
// 	Price   int
// 	Picture string
// }
// const (
// 	quertInsertproduct = "Insert into product(name,price,picture) VALUES($1,$2,$3)"
// 	queryGetproduct   = "Select name,price,picture FROM product where name = $1"
// 	queryDeleteproduct = "Delete FROM product where name = $1"
// 	// queryUpdateProduct= "UPDATE product SET name=$1, price=$2, picture=$3 WHERE name=$4 RETURNING name"
// )
// func (p *Product) Save() error {
// 	stmt, err := postgres.Db.Prepare(quertInsertproduct)
// 	if err != nil {
// 		return err
// 	}
// 	defer stmt.Close()
// 	_, saveErr := stmt.Exec(p.Name, p.Price, p.Picture)
// 	if saveErr != nil {
// 		return saveErr
// 	}
// 	return nil
// }
// // get
// func (p *Product) GetProduct() error {
// 	stmt, err := postgres.Db.Prepare(queryGetproduct)
// 	if err != nil {
// 		return err
// 	}
// 	defer stmt.Close()

// 	result := stmt.QueryRow(p.Name)
// 	getErr := result.Scan(&p.Name, &p.Price, &p.Picture)
// 	if getErr != nil {
// 		return getErr
// 	}
// 	return nil
// }

// // delete
// func (p *Product) Delete() error {
// 	stmt, err := postgres.Db.Prepare(queryDeleteproduct)

// 	if err != nil {
// 		return err
// 	}
// 	defer stmt.Close()

// 	if _, err = stmt.Exec(p.Name); err != nil {
// 		return err
// 	}
// 	return nil
// }

// func (p *Product) Update() error {
// 	stmt, err := postgres.Db.Prepare("UPDATE product SET name = $1, price = $2 WHERE name = $3")

// 	if err != nil {
// 		return err
// 	}
// 	defer stmt.Close()

// 	if _, err = stmt.Exec(p.Name, p.Price, p.Name); err != nil {
// 		return err
// 	}
// 	return nil
// }

// =============================================================================
package model

import postgres "goproject/datastore/Postgress"

type Product struct {
	Pid   int 
	Name   string
	Price   int
	Picture string
}
const (
	quertInsertproduct = "Insert into product(pid,name,price,picture) VALUES($1,$2,$3,$4)"
	queryGetproduct   = "Select pid,name,price,picture FROM product where pid = $1"
	queryDeleteproduct = "Delete FROM product where pid = $1"
	queryUpdateProduct= "UPDATE product SET pid=$1,name=$2, price=$3, picture=$4 WHERE pid=$5 RETURNING pid"
)
func (p *Product) Save() error {
	stmt, err := postgres.Db.Prepare(quertInsertproduct)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, saveErr := stmt.Exec(p.Pid,p.Name, p.Price, p.Picture)
	if saveErr != nil {
		return saveErr
	}
	return nil
}
// get
func (p *Product) GetProduct() error {
	stmt, err := postgres.Db.Prepare(queryGetproduct)
	if err != nil {
		return err
	}
	defer stmt.Close()

	result := stmt.QueryRow(p.Pid)
	getErr := result.Scan(&p.Pid,&p.Name, &p.Price, &p.Picture)
	if getErr != nil {
		return getErr
	}
	return nil
}

// delete
func (p *Product) Delete() error {
	stmt, err := postgres.Db.Prepare(queryDeleteproduct)

	if err != nil {
		return err
	}
	defer stmt.Close()

	if _, err = stmt.Exec(p.Pid); err != nil {
		return err
	}
	return nil
}



// func (p *Product) Update(pid int) error {
// 	stmt, updateErr := postgres.Db.Prepare(queryUpdateProduct)

// 	if updateErr != nil {
// 		return updateErr
// 	}
// 	defer stmt.Close()

// 	_, updateErr = stmt.Exec(p.Pid, p.Name, p.Price,p.Picture, pid)

// 	if updateErr != nil {
// 		return updateErr
// 	}
// 	return nil
// }
func(p *Product)Update(old_pid int) error{
	err:=postgres.Db.QueryRow(queryUpdateProduct, p.Pid, p.Name, p.Price, p.Picture,  old_pid).Scan(&p.Pid)
	return err
}
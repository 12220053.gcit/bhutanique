package postgres

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq" // _ using it indirectly
)

// db info
const (
	postgres_host = "db"
	postgres_port = 5432
	postgres_user = "postgres"
	postgres_password="postgres"
	postgres_dbname ="my_db"
)
// const (
// 	postgres_host = "dpg-ci39hqik728i8tdklo9g-a.singapore-postgres.render.com"
// 	postgres_port = 5432
// 	postgres_user = "postgres_admin"
// 	postgres_password="R2PcmyQWRRuAhMyxD0HN6hEcTR47cHgV"
// 	postgres_dbname ="my_db_ah6v"
// )

var Db *sql.DB

// execute first
func init(){
	db_info :=fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",postgres_host,postgres_port,postgres_user,postgres_password,postgres_dbname)
	var err error
	Db,err=sql.Open("postgres", db_info) //open ge argument(databasedriver, database information-(string))
	if err != nil{
		log.Println(err)
	}else{
		log.Println("Datebase successfully connected")
	}
	
}